<?php
declare(strict_types=1);

/**
 * Copyright (c) 2019 Marian Sievers
 * Licensed under MIT license. See LICENSE.md for more information.
 *
 * InMemoryBookRepository.php of project bokasafn.
 * Created by user marian at 2019-01-09.
 */

namespace Bokasafn\Persistence;


use Bokasafn\Model\Book;
use Bokasafn\Model\BookInterface;

class InMemoryBookRepository implements BookInterface
{

    /**
     * @var Book[]
     */
    private $books;

    public function __construct()
    {
        $this->books = [
            'asdsad-2dasd-1dasdasd' => new Book(),
            'asd9as-90sad-sad2adsd' => new Book()
        ];
    }

    /**
     * @return Book[]
     */
    public function getBooks(): array
    {
        return $this->books;
    }

    /**
     * @param string $uuid
     * @return Book
     */
    public function getBook(string $uuid): Book
    {
        return $this->books[$uuid];
    }
}