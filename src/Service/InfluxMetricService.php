<?php
declare(strict_types=1);

/**
 * Copyright (c) 2019 Marian Sievers
 * Licensed under MIT license. See LICENSE.md for more information.
 *
 * InfluxMetricService.php of project bokasafn.
 * Created by user marian at 2019-01-18.
 */

namespace Bokasafn\Service;


use Clue\React\Buzz\Browser;
use Psr\Http\Message\ResponseInterface;
use Psr\Log\LoggerInterface;
use React\EventLoop\Factory;
use React\EventLoop\LoopInterface;

class InfluxMetricService
{
    /**
     * @var LoopInterface
     */
    private $loop;

    /**
     * @var Browser
     */
    private $browser;

    /**
     * @var LoggerInterface
     */
    private $logger;

    public function __construct(LoggerInterface $logger)
    {
        $this->loop = Factory::create();
        $this->browser = new Browser($this->loop);

        $this->logger = $logger;
    }

    public function writeMetric(string $name, $value, array $tags=[])
    {
        // TODO: add basic tags like host, user or request parameters

        $lineProtocol = sprintf("%s,host=localhost value=%s", $name, $value);

        $this->browser->post('http://localhost:8042/telegraf', ['Content-Type' => 'application/json'], $lineProtocol)->then(
            function(ResponseInterface $response) use ($name) {
                $this->logger->info("Log metric to telegraf", ['metric' => $name]);
            },
            function(ResponseInterface $response) use ($name, $value) {
                $this->logger->warning("Could not log to telegraf", ['metric' => $name, 'value' => $value]);
            }
        );

        $this->loop->run();
    }
}