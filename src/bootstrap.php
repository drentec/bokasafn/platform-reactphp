<?php
declare(strict_types=1);

/**
 * Copyright (c) 2019 Marian Sievers
 * Licensed under MIT license. See LICENSE.md for more information.
 *
 * bootstrap.php of project bokasafn.
 * Created by user marian at 2019-01-12.
 */


require_once dirname(__DIR__) . '/vendor/autoload.php';

// init DI container
$diBuilder = new \DI\ContainerBuilder();

$diBuilder->addDefinitions([
    \Psr\Log\LoggerInterface::class => \DI\create(\DrenTech\Logging\EchoLogger::class)
]);

$container = $diBuilder->build();
$GLOBALS['DI'] = $container;

// init App
$app = $container->get(\Bokasafn\App::class);
$app->setDiContainer($container);

return $app;