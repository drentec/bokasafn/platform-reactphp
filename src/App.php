<?php
declare(strict_types=1);

/**
 * Copyright (c) 2019 Marian Sievers
 * Licensed under MIT license. See LICENSE.md for more information.
 *
 * App.php of project bokasafn.
 * Created by user marian at 2019-01-05.
 */

namespace Bokasafn;


use DI\Container;
use DrenTech\Http\Routing\AutomaticNamespaceRouter;
use DrenTech\Http\Routing\RouteInterface;
use DrenTech\Http\Routing\RouterInterface;
use DrenTech\Middleware\RequestLoggerMiddleware;
use Psr\Http\Message\ServerRequestInterface;
use React\EventLoop\Factory;
use React\EventLoop\LoopInterface;
use React\Http\Middleware\LimitConcurrentRequestsMiddleware;
use React\Http\Server;
use React\Socket\Server as Socket;

class App
{
    /**
     * @var LoopInterface
     */
    private $loop;

    /**
     * @var Socket
     */
    private $socket;

    /**
     * @var RouterInterface
     */
    private $router;

    /**
     * @var Container
     */
    private $diContainer;

    public function __construct(AutomaticNamespaceRouter $router)
    {
        $this->router = $router;
    }

    public function init(): void
    {
        $this->loop = Factory::create();

        $this->socket = new Socket('0.0.0.0:8080', $this->loop);
    }

    public function run(): void
    {

        try {
            $server = new Server(
                [
                    new LimitConcurrentRequestsMiddleware(25),
                    $this->diContainer->get(RequestLoggerMiddleware::class),
                    function (ServerRequestInterface $request) {
                        /** @var RouteInterface $route */
                        $route = $this->router->getRouteForRequest($request);
                        $response = $route->handleRequest($request);

                        return $response;
                    }
                ]
            );

            //TODO: replace echo with logger
            echo "Start listening on " . $this->socket->getAddress() . PHP_EOL;
            $server->listen($this->socket);

            echo "Starting loop..." . PHP_EOL;
            $this->loop->run();

        } catch (\Exception $e) {
            echo "error";
            echo $e->getMessage();
        }
    }

    /**
     * @return Container
     */
    public function getDiContainer(): Container
    {
        return $this->diContainer;
    }

    /**
     * @param Container $diContainer
     */
    public function setDiContainer(Container $diContainer): void
    {
        $this->diContainer = $diContainer;
    }
}