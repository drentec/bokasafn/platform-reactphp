<?php
/**
 * Copyright (c) 2019 Marian Sievers
 * Licensed under MIT license. See LICENSE.md for more information.
 *
 * BookInterface.php of project bokasafn.
 * Created by user marian at 2019-01-09.
 */

namespace Bokasafn\Model;


/**
 * This is an interface so that the model is coupled to a specific backend.
 *
 * This is also so that we can demonstrate how to bind an interface
 * to an implementation with PHP-DI.
 */
interface BookInterface
{
    /**
     * @return Book[]
     */
    public function getBooks(): array;

    /**
     * @param string $uuid
     * @return Book
     */
    public function getBook(string $uuid): Book;
}