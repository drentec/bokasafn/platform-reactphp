<?php
declare(strict_types=1);
/**
 * Copyright (c) 2019 Marian Sievers
 * Licensed under MIT license. See LICENSE.md for more information.
 *
 * HandlerInterface.php of project bokasafn.
 * Created by user marian at 2019-01-05.
 */

namespace DrenTech\Http;


use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

interface HandlerInterface
{
    public function route(): string;

    public function method(): string;

    public function description(): string;

    public function declareInput(): array;  //TODO: add InputTypes return value

    public function handle(RequestInterface $request): ResponseInterface;

    public function cacheTtlSeconds(): int;
}