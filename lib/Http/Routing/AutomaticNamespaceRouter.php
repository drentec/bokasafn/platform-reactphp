<?php
declare(strict_types=1);

/**
 * Copyright (c) 2019 Marian Sievers
 * Licensed under MIT license. See LICENSE.md for more information.
 *
 * AutomaticNamespaceRouter.php of project bokasafn.
 * Created by user marian at 2019-01-12.
 */

namespace DrenTech\Http\Routing;


use Closure;
use DrenTech\Http\Handler\NotFoundHandler;
use DrenTech\Http\HandlerInterface;
use HaydenPierce\ClassFinder\ClassFinder;
use Psr\Http\Message\ServerRequestInterface;

class AutomaticNamespaceRouter implements RouterInterface
{
    const HANDLER_NAMESPACE = "Bokasafn\Handler";

    /**
     * @var RouteInterface[]
     */
    private $routes;

    public function __construct()
    {
        $classes = ClassFinder::getClassesInNamespace(self::HANDLER_NAMESPACE, ClassFinder::RECURSIVE_MODE);

        foreach ($classes as $class) {
            //TODO: get class via DI container
            //$handler = new $class();
            $handler = $GLOBALS['DI']->get($class);
            if($handler instanceof HandlerInterface) {
                /** @var HandlerInterface $handler */
                $this->routes[$handler->route()] = new HttpRoute($handler);
            }
        }
    }

    public function getRoutes(): array
    {
        return $this->routes;
    }

    public function getRouteForPath(string $path): RouteInterface
    {
        if(array_key_exists($path, $this->routes)) {
            return $this->routes[$path];
        } else {
            return new HttpRoute(new NotFoundHandler());
        }

    }

    public function getHandlerForPath(string $path): Closure
    {
        /** @var RouteInterface $route */
        $route = $this->routes[$path];

        return $route->getHandlerFunction();
    }

    public function getRouteForRequest(ServerRequestInterface $request): RouteInterface
    {
        $pathString = $request->getRequestTarget();
        return $this->getRouteForPath($pathString);
    }
}