<?php
declare(strict_types=1);

/**
 * Copyright (c) 2019 Marian Sievers
 * Licensed under MIT license. See LICENSE.md for more information.
 *
 * HttpRoute.php of project bokasafn.
 * Created by user marian at 2019-01-06.
 */

namespace DrenTech\Http\Routing;


use Closure;
use DrenTech\Http\HandlerInterface;
use DrenTech\Http\HttpMethod;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

class HttpRoute implements RouteInterface
{
    /**
     * @var HandlerInterface
     */
    private $handler;

    /**
     * HttpRoute constructor.
     * @param HandlerInterface $handler
     */
    public function __construct(HandlerInterface $handler)
    {
        $this->handler = $handler;
    }


    public function getMethod(): string
    {
        //TODO: check if HttpMethod
        return $this->handler->method();
    }

    public function getPath(): string
    {
        return $this->handler->route();
    }

    public function getHandlerFunction(): Closure
    {
        return Closure::fromCallable([$this->handler, "handle"]);
    }

    public function handleRequest(RequestInterface $request): ResponseInterface
    {
        return $this->handler->handle($request);
    }
}