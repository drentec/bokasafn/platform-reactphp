<?php
declare(strict_types=1);

/**
 * Copyright (c) 2019 Marian Sievers
 * Licensed under MIT license. See LICENSE.md for more information.
 *
 * RouteInterface.php of project bokasafn.
 * Created by user marian at 2019-01-06.
 */

namespace DrenTech\Http\Routing;


use Closure;
use DrenTech\Http\HttpMethod;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

interface RouteInterface
{
    public function getMethod(): string;

    public function getPath(): string;

    public function getHandlerFunction(): Closure;

    public function handleRequest(RequestInterface $request): ResponseInterface;
}