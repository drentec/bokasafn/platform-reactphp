<?php
declare(strict_types=1);

/**
 * Copyright (c) 2019 Marian Sievers
 * Licensed under MIT license. See LICENSE.md for more information.
 *
 * ManualRouter.php of project bokasafn.
 * Created by user marian at 2019-01-12.
 */

namespace DrenTech\Http\Routing;


use Closure;
use Psr\Http\Message\ServerRequestInterface;

class ManualRouter implements RouterInterface
{

    public function getRoutes(): array
    {
        // TODO: Implement getRoutes() method.
    }

    public function getRouteForPath(string $path): RouteInterface
    {
        // TODO: Implement getRouteForPath() method.
    }

    public function getHandlerForPath(string $path): Closure
    {
        // TODO: Implement getHandlerForPath() method.
    }

    public function getRouteForRequest(ServerRequestInterface $request): RouteInterface
    {
        // TODO: Implement getRouteForRequest() method.
    }
}