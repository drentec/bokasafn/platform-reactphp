<?php
declare(strict_types=1);

/**
 * Copyright (c) 2019 Marian Sievers
 * Licensed under MIT license. See LICENSE.md for more information.
 *
 * RouterInterface.php of project bokasafn.
 * Created by user marian at 2019-01-06.
 */

namespace DrenTech\Http\Routing;


use Closure;
use Psr\Http\Message\ServerRequestInterface;

interface RouterInterface
{
    public function getRoutes(): array;

    public function getRouteForPath(string $path): RouteInterface;

    public function getHandlerForPath(string $path): Closure;

    public function getRouteForRequest(ServerRequestInterface $request): RouteInterface;
}