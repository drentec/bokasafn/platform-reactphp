<?php
declare(strict_types=1);

/**
 * Copyright (c) 2019 Marian Sievers
 * Licensed under MIT license. See LICENSE.md for more information.
 *
 * HttpMethod.php of project bokasafn.
 * Created by user marian at 2019-01-06.
 */

namespace DrenTech\Http;


use MyCLabs\Enum\Enum;

class HttpMethod extends Enum
{
    const __default = self::GET;

    const GET = "GET";

    const POST = "POST";

    const DELETE = "DELETE";

    const UPDATE = "UPDATE";

    const PATCH = "PATCH";
}