<?php
/**
 * Copyright (c) 2019 Marian Sievers
 * Licensed under MIT license. See LICENSE.md for more information.
 *
 * ReactMiddlewareInterface.php of project bokasafn.
 * Created by user marian at 2019-01-07.
 */

namespace DrenTech\Middleware;


use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

interface ReactMiddlewareInterface
{
    /**
     * Handles the resolve call of the middleware.
     *
     * @param ServerRequestInterface $request
     * @param callable $next
     * @return ResponseInterface
     */
    public function __invoke(ServerRequestInterface $request, callable $next): ResponseInterface;
}