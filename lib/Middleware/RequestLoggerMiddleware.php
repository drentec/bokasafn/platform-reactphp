<?php
declare(strict_types=1);

/**
 * Copyright (c) 2019 Marian Sievers
 * Licensed under MIT license. See LICENSE.md for more information.
 *
 * RequestLoggerMiddleware.php of project bokasafn.
 * Created by user marian at 2019-01-06.
 */

namespace DrenTech\Middleware;


use DrenTech\Logging\EchoLogger;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Log\LoggerAwareTrait;
use Psr\Log\LoggerInterface;
use function React\Promise\resolve;

class RequestLoggerMiddleware implements ReactMiddlewareInterface
{
    /**
     * @var LoggerInterface
     */
    private $logger;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * Handles the resolve call of the middleware.
     *
     * @param ServerRequestInterface $request
     * @param callable $next
     * @return ResponseInterface
     * @throws \Throwable
     *
     */
    public function __invoke(ServerRequestInterface $request, callable $next): ResponseInterface
    {
        $this->logger->info(sprintf("[%s] %s", $request->getMethod(), $request->getUri()));

        try {
            $response = $next($request);
        } catch (\Exception $e) {
            throw $e;
        } catch (\Throwable $e) {
            // handle Errors just like Exceptions (PHP 7+ only)
            throw $e;
        }

        return $response;
    }
}