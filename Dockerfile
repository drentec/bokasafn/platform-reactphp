FROM php:7.2-alpine AS base

MAINTAINER Marian Sievers <info@mariansievers.de>

# Customize any core extensions here
#RUN apt-get update && apt-get install -y \
#        libfreetype6-dev \
#        libjpeg62-turbo-dev \
#        libmcrypt-dev \
#        libpng12-dev \
#    && docker-php-ext-install -j$(nproc) iconv mcrypt \
#    && docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/ \
#    && docker-php-ext-install -j$(nproc) gd

# install dependencies
RUN apk add curl-dev composer

# install PHP extensions
RUN docker-php-ext-install iconv curl json

# COPY config/php.ini /usr/local/etc/php/
COPY . /srv/app

WORKDIR "/srv/app"
CMD php public/index.php

FROM base AS development

RUN apk add nodejs npm
RUN npm install -g nodemon

CMD nodemon --config nodemon.json public/index.php