################
# build docker #
################
push_docker:
	docker login registry.gitlab.com
	docker build --target base  -t registry.gitlab.com/dren-tech/bokasafn/platform/php .
	docker push registry.gitlab.com/dren-tech/bokasafn/platform/php

build:
	docker-compose build

#########################
# dependency management #
#########################
install:
	docker-compose run app composer install --prefer-dist

update:
	docker-compose run app composer update --prefer-dist

#############################
# run tasks for development #
#############################
start:
	docker-compose up -d

start-foreground:
	docker-compose up

stop:
	docker-compose stop

########
# misc #
########